package com.pinnaclegallery.gallery.themes;

import com.pinnaclegallery.gallery.R;

public class BlackTheme extends DarkTheme {

    @Override
    public int getBackgroundColorRes() {
        return R.color.black_bg;
    }
}
