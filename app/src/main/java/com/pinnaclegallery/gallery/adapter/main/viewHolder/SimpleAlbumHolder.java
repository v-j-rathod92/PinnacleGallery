package com.pinnaclegallery.gallery.adapter.main.viewHolder;

import android.view.View;
import android.widget.ImageView;

import com.pinnaclegallery.gallery.R;
import com.pinnaclegallery.gallery.data.models.Album;
import com.pinnaclegallery.gallery.ui.widget.ParallaxImageView;

public class SimpleAlbumHolder extends AlbumHolder {

    public SimpleAlbumHolder(View itemView) {
        super(itemView);
    }

    @Override
    public void setAlbum(Album album) {
        super.setAlbum(album);
        final ImageView image = itemView.findViewById(R.id.image);
        if (image instanceof ParallaxImageView) {
            ((ParallaxImageView) image).setParallaxTranslation();
        }
        loadImage(image);
    }
}
