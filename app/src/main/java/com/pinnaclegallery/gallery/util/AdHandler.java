package com.pinnaclegallery.gallery.util;

import android.content.Context;
import android.util.Log;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;
import com.pinnaclegallery.gallery.R;
import com.pinnaclegallery.gallery.data.Settings;

/**
 * Created by rgi-40 on 22/11/17.
 */

public class AdHandler {

    private static AdHandler adHandler;
    private static InterstitialAd fullScreenAd;
    private static Settings settings;
    private static Context context;

    public static AdHandler getInstance(Context mContext) {
        if (adHandler == null) {
            adHandler = new AdHandler();
            context = mContext;
            settings = Settings.getInstance(mContext);
            fullScreenAd = new InterstitialAd(mContext);
            fullScreenAd.setAdUnitId(mContext.getString(R.string.admob_fullscreen_ad_id));
            registerListener();
        }

        return adHandler;
    }

    public static void loadAd() {
        fullScreenAd.loadAd(new AdRequest.Builder().build());
    }

    public static void show() {
        if (fullScreenAd.isLoaded()) {
            fullScreenAd.show();
        }
    }

    private static void registerListener() {
        fullScreenAd.setAdListener(new AdListener() {
            @Override
            public void onAdClosed() {
                loadAd();
            }
        });
    }

    public static void clickPerformed() {
        int totalClick = settings.getTotalClick(context);
        if (totalClick == 10) {
            show();
            settings.resetClick(context);
        } else {
            settings.addNewClick(context);
        }
    }
}
